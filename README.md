## JARduino  

Aplicación creada para establecer comunicación Serial (arduino, ESP, bluetooth, etc).  
De forma rápida, sin necesidad de cargar programas *'pesados'* o un IDE.  
Util para lectura del puerto serial,  o testear alguna respuesta del microcontrolador.
  
Descargas:  
- [Instalable (JRE)](https://drive.google.com/drive/folders/1CTUEWgKrnmwVpzwWy5cdtpmkGBE4jC9B?usp=sharing)
- [Instalable (JRE-JDK)](https://drive.google.com/drive/folders/16UYr7QtjUDnVN8jFD80o5tPCmkCntoMS?usp=sharing)
- [Jar Ejecutable+libreria](https://drive.google.com/drive/folders/1BeTCSQGqYx-Ox-ys0wpZyAjvPapq0O7R?usp=sharing)
  
Estructura de directorios
```
📂️ jarduino
├── 📂️ lib
│   └── ☕️ PanamaHitek_Arduino-3.0.0.jar
└── ☕️ jarduino.jar
```

```bash
java -jar JARduino.jar
```  
> probado en **GNU/Linux** & Windows *(click derecho: ejecutar con JRE)*  
