package javarduino;        /**         @author jp.av.dev@gmail.com         */

import com.panamahitek.ArduinoException;
import javax.swing.DefaultComboBoxModel;
import com.panamahitek.PanamaHitek_Arduino;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.text.DefaultCaret;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

public class Jarduino extends javax.swing.JFrame {
    
    private DefaultComboBoxModel modelPuertos;
    private final PanamaHitek_Arduino ino = new PanamaHitek_Arduino();
    String PuertoCOM;
    String TipoConexion;
    int baudio;
    SerialPortEventListener arduinoListener;
    
    public Jarduino() {
        this.arduinoListener = (SerialPortEvent spe) -> {
            try {
                if (ino.isMessageAvailable()) {
                    txtRX.setText(txtRX.getText()+"\n"+ino.printMessage());
                }
            } catch (SerialPortException | ArduinoException ex) {
                JOptionPane.showMessageDialog(rootPane, "Error de comunicación", "Error", 0);
            }
        };
        setBaudio(9600);
        actualizarPuertos();
        initComponents();
        super.setLocationRelativeTo(null);
        rBotones.add(rRX);
        rBotones.add(rTX);
        rBotones.add(rRXTX);
        rRX.setSelected(true);
        TipoConexion = "RX";
        // Para activar el seguimiento del texto con AutoScroll
        DefaultCaret caret = (DefaultCaret) txtRX.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }

    public int getBaudio() {
        return baudio;
    }

    private void setBaudio(int baudio) {
        this.baudio = baudio;
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents//GEN-BEGIN:initComponents
    private void initComponents() {

        rBotones = new javax.swing.ButtonGroup();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        cbPuertos = new javax.swing.JComboBox<>();
        btnConectar = new javax.swing.JButton();
        btnDesconectar = new javax.swing.JButton();
        btnActualizar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        rTX = new javax.swing.JRadioButton();
        rRX = new javax.swing.JRadioButton();
        rRXTX = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        btnLimpRX = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtTX = new javax.swing.JTextField();
        btnTX = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtRX = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("JARduino v1.0");
        setLocation(new java.awt.Point(500, 150));
        setResizable(false);

        jLabel1.setText("Seleciona un Puerto  :");

        cbPuertos.setModel(modelPuertos);

        btnConectar.setText("Conectar");
        btnConectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConectarActionPerformed(evt);
            }
        });

        btnDesconectar.setText("Desconectar");
        btnDesconectar.setEnabled(false);
        btnDesconectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesconectarActionPerformed(evt);
            }
        });

        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        rTX.setText("TX");
        rTX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rTXActionPerformed(evt);
            }
        });

        rRX.setText("RX");
        rRX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rRXActionPerformed(evt);
            }
        });

        rRXTX.setText("RXTX");
        rRXTX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rRXTXActionPerformed(evt);
            }
        });

        jLabel2.setText("Conexión RX (Escuchar)");

        btnLimpRX.setText("Limpiar");
        btnLimpRX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpRXActionPerformed(evt);
            }
        });

        jLabel3.setText("Conexión TX (Transmitir)");

        txtTX.setEnabled(false);
        txtTX.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTXKeyPressed(evt);
            }
        });

        btnTX.setText("Enviar");
        btnTX.setEnabled(false);
        btnTX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTXActionPerformed(evt);
            }
        });

        jScrollPane2.setAutoscrolls(true);
        jScrollPane2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane2.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);

        txtRX.setEditable(false);
        txtRX.setColumns(35);
        txtRX.setRows(5);
        jScrollPane2.setViewportView(txtRX);

        jMenu1.setText("Baudio");

        jMenu3.setText("Tasa(Rate)");

        jMenuItem3.setText("300");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        jMenuItem4.setText("1200");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem4);

        jMenuItem5.setText("2400");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem5);

        jMenuItem6.setText("4800");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem6);

        jMenuItem7.setText("9600");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem7);

        jMenuItem8.setText("19200");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem8);

        jMenuItem9.setText("38400");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem9);

        jMenuItem10.setText("57600");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem10);

        jMenuItem11.setText("74880");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem11);

        jMenuItem12.setText("115200");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem12);

        jMenuItem13.setText("230400");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem13);

        jMenuItem14.setText("250000");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem14);

        jMenuItem15.setText("500000");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem15);

        jMenuItem16.setText("1000000");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem16);

        jMenuItem17.setText("2000000");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem17);

        jMenu1.add(jMenu3);

        jMenuItem18.setText("Especificar");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem18);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Ayuda");

        jMenuItem1.setText("Ayuda");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuItem2.setText("Info");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtTX)
                            .addComponent(jSeparator2)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnConectar, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnDesconectar, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cbPuertos, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(btnActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rRX)
                                    .addComponent(rRXTX)
                                    .addComponent(rTX)))
                            .addComponent(jSeparator1))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLimpRX, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(82, 82, 82))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnTX, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(86, 86, 86))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbPuertos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnConectar)
                            .addComponent(btnDesconectar)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(rRX)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rTX)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rRXTX))
                    .addComponent(btnActualizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLimpRX)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTX)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtTX, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );

        jScrollPane2.getAccessibleContext().setAccessibleName("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnConectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConectarActionPerformed
            try {
                PuertoCOM = cbPuertos.getSelectedItem().toString();
                conectar(TipoConexion);
                super.setTitle("JARduino v1.0 ("+baudio+")");
            } catch (Exception e){
                JOptionPane.showMessageDialog(rootPane, "Debes seleccionar un puerto", "Puerto COM?", JOptionPane.INFORMATION_MESSAGE);
            }
    }//GEN-LAST:event_btnConectarActionPerformed//GEN-LAST:event_btnConectarActionPerformed

    private void btnDesconectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesconectarActionPerformed
        try {
            ino.killArduinoConnection();
            btnDesconectar.setEnabled(false);
            btnConectar.setEnabled(true);
            rRX.setEnabled(true);
            rTX.setEnabled(true);
            rRXTX.setEnabled(true);
            cbPuertos.setEnabled(true);
            //rBotones.clearSelection();
            txtTX.setText("");
            txtTX.setEnabled(false);
            btnTX.setEnabled(false);
            btnActualizar.setEnabled(true);
            super.setTitle("JARduino v1.0");
        } catch (ArduinoException ex) {
            JOptionPane.showMessageDialog(rootPane, "Error al intentar desconectar\n¿Existen conexiones activas?", "Error", 0);
        }
    }//GEN-LAST:event_btnDesconectarActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        actualizarPuertos();
        cbPuertos.setModel(modelPuertos);
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void rRXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rRXActionPerformed
        TipoConexion = "RX";
    }//GEN-LAST:event_rRXActionPerformed

    private void rTXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rTXActionPerformed
        TipoConexion = "TX";
    }//GEN-LAST:event_rTXActionPerformed

    private void rRXTXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rRXTXActionPerformed
        TipoConexion = "RXTX";
    }//GEN-LAST:event_rRXTXActionPerformed

    private void btnLimpRXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpRXActionPerformed
        txtRX.setText("");
    }//GEN-LAST:event_btnLimpRXActionPerformed

    private void btnTXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTXActionPerformed
        enviar();
        txtTX.requestFocus();
    }//GEN-LAST:event_btnTXActionPerformed

    private void txtTXKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTXKeyPressed
        if (evt.getKeyCode() == 10 ) {
            enviar();
        }
    }//GEN-LAST:event_txtTXKeyPressed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        JOptionPane.showMessageDialog(rootPane, "App Comunicación Serial (jp.av.dev@gmail.com)","Acerca de JARduino v1.0", -1);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        setBaudio(2400);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        setBaudio(9600);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
        String baudioQ = JOptionPane.showInputDialog("Ingresar valor (Rate Baudio)");
        if (baudioQ != null) {
            try {
                int baudioP = Integer.parseInt(baudioQ);
                setBaudio(baudioP);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(rootPane, "Debes ingresar solo números", "Error", 0);
            }
        }
    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        setBaudio(1200);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        setBaudio(300);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        setBaudio(4800);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        setBaudio(19200);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        setBaudio(38400);
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        setBaudio(57600);
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        setBaudio(74880);
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        setBaudio(115200);
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        setBaudio(230400);
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        setBaudio(250000);
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        setBaudio(500000);
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        setBaudio(1000000);
    }//GEN-LAST:event_jMenuItem16ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        setBaudio(2000000);
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        JOptionPane.showMessageDialog(rootPane, "Para utilizar esta aplicación, debes conectar\nel microcontrolador, presionar actualizar\n y seleccionar el puerto COM correspondiente.\nSeleccionar el tipo de conexión:\nRX, para Lectura. TX, para transmisión.\nRXTX para ambas opciones.\n\nLa configuración por defecto es a 9600 baudio\nPuedes modificar el valor en el menú \"Baudio\".\nLa tasa selecionada se muestra en el título\nal iniciar la conexión.","Ayuda JARduino", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Jarduino.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(() -> {
            new Jarduino().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnConectar;
    private javax.swing.JButton btnDesconectar;
    private javax.swing.JButton btnLimpRX;
    private javax.swing.JButton btnTX;
    private javax.swing.JComboBox<String> cbPuertos;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.ButtonGroup rBotones;
    private javax.swing.JRadioButton rRX;
    private javax.swing.JRadioButton rRXTX;
    private javax.swing.JRadioButton rTX;
    private javax.swing.JTextArea txtRX;
    private javax.swing.JTextField txtTX;
    // End of variables declaration//GEN-END:variables//GEN-END:variables

    private void actualizarPuertos() {
        // Creación ComboModel y carga de puertos disponibles.
        modelPuertos = new DefaultComboBoxModel();
        List<String> puertos = ino.getSerialPorts();
        for (int i = 0; i < puertos.size(); i++) {
            modelPuertos.addElement(puertos.get(i));
        }
    }

    private void conectar(String TipoConexion) {
        switch (TipoConexion) {
            case "RX":
                try {
                    ino.arduinoRX(PuertoCOM, baudio, arduinoListener);
                    btnDesconectar.setEnabled(true);
                    btnConectar.setEnabled(false);
                    rRX.setEnabled(false);
                    rTX.setEnabled(false);
                    rRXTX.setEnabled(false);
                    cbPuertos.setEnabled(false);
                    btnLimpRX.setEnabled(true);
                    txtRX.setEnabled(true);
                    txtRX.setText("");
                    btnTX.setEnabled(false);
                    txtTX.setEnabled(false);
                    btnActualizar.setEnabled(false);
                } catch (ArduinoException | SerialPortException ex) {
                    JOptionPane.showMessageDialog(rootPane, "Error al iniciar la conexión\nPuerto: "+PuertoCOM+"\nBaudio : "+baudio);
                }
                break;
            case "TX":
                try {
                    ino.arduinoTX(PuertoCOM, baudio);
                    btnDesconectar.setEnabled(true);
                    btnConectar.setEnabled(false);
                    rRX.setEnabled(false);
                    rTX.setEnabled(false);
                    rRXTX.setEnabled(false);
                    cbPuertos.setEnabled(false);
                    btnLimpRX.setEnabled(false);
                    txtRX.setEnabled(false);
                    txtRX.setText("");
                    btnTX.setEnabled(true);
                    txtTX.setEnabled(true);
                    txtTX.setText("");
                    btnActualizar.setEnabled(false);
                } catch (ArduinoException ex) {
                    JOptionPane.showMessageDialog(rootPane, "Error al iniciar la conexión\nPuerto: "+PuertoCOM+"\nBaudio : "+baudio);
                }
                break;
            case "RXTX":
                try {
                    ino.arduinoRXTX(PuertoCOM, baudio, arduinoListener);
                    btnDesconectar.setEnabled(true);
                    btnConectar.setEnabled(false);
                    rRX.setEnabled(false);
                    rTX.setEnabled(false);
                    rRXTX.setEnabled(false);
                    cbPuertos.setEnabled(false);
                    btnLimpRX.setEnabled(true);
                    txtRX.setEnabled(true);
                    txtRX.setText("");
                    btnTX.setEnabled(true);
                    txtTX.setEnabled(true);
                    txtTX.setText("");
                    btnActualizar.setEnabled(false);
                } catch (ArduinoException ex) {
                    JOptionPane.showMessageDialog(rootPane, "Error al iniciar la conexión\nPuerto: "+PuertoCOM+"\nBaudio : "+baudio);
                }
                break;
            default:
                JOptionPane.showMessageDialog(rootPane, "Error al seleccionar tipo de conexión", "Error", 0);
        }
    }

    void enviar() {
        try {
            ino.sendData(txtTX.getText());
            txtTX.setText("");
        } catch (ArduinoException | SerialPortException ex) {
           JOptionPane.showMessageDialog(rootPane, "Error! No es posible conectar con Arduino", "Sin Conexión", 0);
        }
    } 
    
}
